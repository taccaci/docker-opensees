set -x
WRAPPERDIR=$( cd "$( dirname "$0" )" && pwd )

# Run the script with the runtime values passed in from the job request

docker run -i --sig-proxy=true --rm \
			--memory="${AGAVE_JOB_MEMORY_PER_NODE}g" \
			-v $WRAPPERDIR:/data \
			stevemock/docker-opensees /bin/sh -c "OpenSees < /data/${dataset}"

if [ ! $? ]; then
	echo "Docker container exited with an error status. $?" >&2
	${AGAVE_JOB_CALLBACK_FAILURE}
	exit
fi

#docker rmi stevemock/docker-opensees
