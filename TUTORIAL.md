# Agave End-To-End Example
*******

## Big Picture

In this example, you will start from a clean slate and run a job on your own systems by forking the process rather than using a traditional batch scheduler. This is a suitable approach when using dedicated resources such as personal servers, virtual machines, and the head nodes of clusters when the job is not significantly computationally expensive.

We will walk though 4 steps using the Agave CLI to carry out the interactions with Agave and show us the actual API calls needed to perform the various tasks. All job data and json definitions are included in this example. The included sample data strcuture is as follows:

  + app
    + opensees-2.4.4.5804
      + test
        - test.sh : placeholder file for testing the app's execution
      - app.json : json definition of our app
      - wrapper.sh : wrapper script to accept arguments from agave and start the job.
  + html : sample webapp to run the example app. Run on a local web server, ie. localhost
  + inputs: directory of inputs you can use to test with
  + job
    - submit.json : bare-bones job submission request to run our example app
    - submit-full.json : full job submission request to run our example app and get notifications
  + system
    - execution.json : json definition of our execution system
    - storage.json : json definition of our storage system
  - TUTORIAL.md : this file

NOTE: If you have not yet created an iPlant account, you will need to do so to complete this exercise. You can create a free account in the [iPlant User Portal](https://user.iplantc.org).

## Download the Agave CLi

In this tutorial we will use the Agave CLI to do the heavy lifting for us. Before we begin, please download the CLI and initialize it to use iPlant's tenant. From your terminal, execute the following commands to clone the CLI from our Git repository and add it to your default system path.

	> git clone https://bitbucket.org/taccaci/foundation-cli.git agave-cli
	> cd agave-cli
	> export PATH=`pwd`/bin:$PATH

Now initialize the CLI to use the iPlant tenant.

	> tenants-init -t 1

The CLI is now pointing at iPlant's tenant of Agave. Next we need to obtain a set of API keys. If you have not yet gotten an iPlant user account, please visit the [iPlant User Portal](https://user.iplantc.org) and do so now. You will not be able to continue without one.

## Get your client keys

All the Agave APIs use OAuth2 for authentication and authorization decisions. OAuth2 can be a little painful to work with if you're not familiar with it, so we've built a couple simple commands into the CLI to make this process easier. To get started, we need to create a set of API keys so Agave knows who we are.

	> clients-create -u $API_USERNAME -p $API_PASSWORD -N cli-client -S -V

Once the command completes, you will have a shiny new set of API keys stored in your local system for use from here on. This is a one-time action, so you won't need to generate another set of API keys as long as you use the CLI.

## Authenticate

In the last step, we got a set of API keys. Now we need to login an get a valid access token to communicate with the rest of the APIs. Unlike API keys, access tokens do expire after about 4 hours. We can, however refresh them whenever we need to. Let's see how this is done.

	> auth-tokens-create -S -V -u $API_USERNAME -p $API_PASSWORD

Now that we have a valid token, let's see how we can renew it if need be.

	> auth-tokens-refresh -S

That's it. Now we have a refreshed token good for another 4 hours.

We've got all the prerequisites done, so let's get to the actual exercise. First up, adding some of our own systems.

## Register your systems

As an iPlant user, you already have storage and compute resources available to you, but in this exercise, we want to use our own resources for everything, so we will start off by defining a storage and execution system with Agave. In this example, both systems will be simple VMs that we have access to. If you are following along, you should make a few changes to the files so they will work for you.

1. Update the system ids with names appropriate to your use. ie. nryan.compute.azure
2. Update the `storage.host` and `login.host` values to the hostnames or ip addresses of your actual systems.
3. Update the `storage.port` and `login.port` values to the ports of your actual systems.
4. Update the `storage.auth` and `login.auth` stanzas with the auth info for your systems. For more info on what auth mechanisms are supported, consult the [Agave Systems Management Tutorial](http://preview.agaveapi.co/system-management/).

```If you do not already have a system you can use, you can spin up a free VM on iPlant's private science cloud, [Atmosphere](http://atmo.iplantc.org).```

Registering the systems is simply a matter of sending a POST to the Systems service. We will use Agave's CLI to do this for us. First let's register our execution system. If you have not changed the system id and auth info, this is the time to do it.

	> systems-addupdate -V -F systems/execution.json

Notice in the response that the auth information is not returned. Once you submit your system description, any authentication information is sliced up, encrypted and never output again.

One thing you might notice in the system description is that we specified a queue even though our system is not using a scheduler. While this is optional, it allows us to place some quotas on the apps and jobs that we run on the system. For example, we specified a `maxUserJobs` of 10, thus we can throttle individual users from having more than 10 personal jobs running on our system at once. Ditto for `maxJobs`.

Now let's register the storage system. Again, if you have not changed the system id and auth info, this is the time to do it.

	> systems-addupdate -V -F systems/storage.json

If you ever need to updte the system description, you can edit your description and run the came command. If you simply need to review the system description, you can query the Systems service using the system id you gave your system.

	> systems-list -V storage.example.com
	> systems-list -V execute.example.com

Now we have some systems, let's push up some data.

## Add an app

Apps in Agave can be as simple or complicated as you want to make them. In this exercise, our app runs the Unix `head` utility. `head` is installed in most Linux servers by default, so it's a pretty safe bet you have it already installed on your system and a good executable for this example.

The `head` command takes two optional arguments and one mandatory input file. For this exercise, we only care about one of the inputs, so that's all we will tell Agave about. Also, we will add a simple validator to our app's input file restricting the type of input to just txt files. This isn't manadatory, but it shows how you might add validations appropriate to your app in the future.

As before, ff you are following along, you should make a few changes to the files so they will work for you.

1. Update the app name to something appropriate to you. ie. nryan-head-primer
2. Update the deploymentSystem value to your storage system id
3. Update the executionSystem value to your execution system id

Once you make those updated, post your app description to the Apps service.

	> apps-addupdate -V -F app/head-1.00/app.json

Why did this call fail? The error message says that it failed because it could not file the `deploymentPath` you specified. Agave doesn't assume that your application's dependencies are already on the `executionSystem`. In fact, Agave doesn't care where they may be located. All it needs is a  executable script that it can inject your app's inputs and parameters to, stage to the execution system, and run. That script can be a simple shell script or something more elaborate. All that really matters is that the script is located in the `deploymentPath` folder on the `deploymentSystem` system with a relative path of `templatePath`. At run time, the entire `deploymentPath` folder will be staged to your job's work directory, so any dependencies you have can be placed in there and safely assumed to be present at run time.

So, back to the error message. It says it could not find the `deploymentPath`, so let's copy our app folder up to our storage system as defined in our app description.

	> files-upload -S storage.example.com -F app

Just to make sure, let's make sure the folder is there

	> files-list -S storage.example.com app

And because we're paranoid, let's make sure the wrapper script is there

	> files-list -S storage.example.com app/head-1.00/wrapper.sh

Now that our app is somewhere Agave can find it, let's try registering the app again.

	> apps-addupdate -V -F app/head-1.00/app.json

## Run a job

Thus far we have registered a storage and execution system, uploaded some data, and registered an app. All that's left is to run our job. Looking in the job description in job/submit.json, you will see a pretty basic job request. We specify a name for our job, the app we want to run, the parameter and input we care about, and tell it to skip achiving the output. Update the `appId` to match the `name`-`version` of your app then run the following commands to upload the sample input file and run the job.

	> files-upload -F inputs/picksumipsum.txt -S storage.example.com /
	> jobs-submit -V -W -F jobs/submit.json

In the simple job we submitted, we didn't ask Agave for any notifications when our job ends. This is a pretty simple job that will run almost immediately, so we just added the -W option to the submit command, which will tell the CLI to keep checking the job status every few seconds until it completes. It should be done any second now.

Since we did not archive our data, it's still where we left it in the job folder on our execution system. You probably have no idea where that is, so let's use the CLI to find this out. Below, we use `$JOB_ID` to represent the id of the job we just submitted.

	> jobs-output -V $JOB_ID

We can also download the job output directly.

	> jobs-output -V -D -P head_output.txt $JOB_ID

The other job description in job/submit-full.json will run the exact same app, but describes all the available options a job request can have.

* **queue:** which queue to use. uses default queue if not specified.
* **nodeCount:** how many nodes to use...always 1 on forked/cli systems
* **maxMemoryPerNode:** up to the max memory in the default queue
* **processorsPerNode:** up to the max ppn of the default queue
* **requestedTime:** up to the max requeste time of the default queue or 1000 hours
* **notifications:** describes where and when notifications should be sent during the lifetime of a job.
	* **url:** where to send the notification. URL or email address. If you want to test out the webhook (URL) feature, you can create a free [Request Bin](http://requestb.in/).
	* **event:** event which will trigger the notification
	* **persistent:** true if the notification can be sent more than once

Update the `appId` to match the `name`-`version` of your app then run the following command.

	> jobs-submit -V -F jobs/submit-full.json

Since we're getting notifications on this job, we won't stalk it. Our email client will tell us when it completes. Once done, we can view the finished job description. We use `$JOB_ID` to represent the id of the job we just submitted in the following commands.

	> jobs-list -V $JOB_ID

In the last job, we used the `jobs-output` command to view our job output. We could do the same thing here, but in our job description this time we requested the output to be archived on our storage system in the `archive/jobs/head-demo-2` folder. Rather than use the job service, we can use the Files service to view our job output.

	> files-list -S storage.example.com archive/jobs/head-demo-2
	> files-get -S storage.example.com archive/jobs/head-demo-2/head_output.txt

## Sample Web App

Inside the `html` folder is a simple web application to run a job with the app from this exercise. The webapp is a simple html + css + jQuery application that should be relatively easy to follow. The app uses OAuth2 to log you in and fetch a token from Agave. The only restriction is that the app must run on a local web server (ie. localhost). It is not meant for production deployment.
